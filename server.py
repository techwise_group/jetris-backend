from gtcp import server
import scripts

PORT = 42069
s = server(PORT, print(f"Listening on port {PORT}"))

def connectionHandler(socket):
    socket.on("login", scripts.auth.login)
    socket.on("signup", scripts.auth.signup)
    socket.on("getuserinfo", scripts.misc.getuserinfo)
    socket.on("endranked", scripts.matchmaking.endranked)
    socket.on("leavequeue", scripts.matchmaking.leavequeue)
    socket.on("rankedqueue", scripts.matchmaking.rankedqueue)
    socket.on("unrankedqueue", scripts.matchmaking.unrankedqueue)
s.connection(connectionHandler)
