from scripts import db
from scripts.misc import numlengen
from hashlib import sha512
from uuid import uuid4

def signup(username, password, callback):
    if db["users"].find_one({"username": username}):
        callback(0, "Username is taken")
    else:
        id = ""
        taken = True
        while taken:
            id = str(uuid4())
            taken = db["users"].find_one({"_id": id}, {})
        salt = numlengen(10)
        db["users"].insert_one({
            "_id": id, # taking a wild guess that this is usertoken used in rooms folder
            "username": username,
            "password": sha512(bytearray(password + salt, 'utf-8')).hexdigest(),
            "salt": salt,
            "win": 0,
            "loss": 0,
            "draw": 0,
            "elo": 1500,
            "opponents_total_rating": 0 # might be able to replace with jpp since idk wtf jpp is. this is used to calculate user rating and also a cool statistic that only goes up ;)
        })
        callback(1, id)
