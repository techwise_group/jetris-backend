from hashlib import sha512
from scripts import db

def login(username, password, callback):
    query = db["users"].find_one({"username": username})
    if not query:
        callback(0, "No user with that username exists")
    elif not sha512(bytearray(password + query["salt"], 'utf-8')).hexdigest() == query["password"]:
        callback(0, "Incorrect password")
    else:
        callback(1, query["_id"])
    print(username, password)
