from random import randint

def numlengen(length):
    string = ""
    for i in range(length):
        string += ("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890")[randint(0, 61)]
    return string