import pymongo

global db
db = pymongo.MongoClient("mongodb://localhost/27017")["jetris"]

import scripts.misc
import scripts.auth
import scripts.matchmaking