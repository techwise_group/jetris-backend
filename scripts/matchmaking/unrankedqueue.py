from scripts.matchmaking import iuq

def unrankedqueue(id, ip, callback):
    global iuq
    if iuq:
        callback(*iuq[:-1])
        iuq[2](id, ip)
        iuq = None
    else:
        iuq = [id, ip, callback]