from scripts import db

def calculaterating(user_id):
    user_id = {"_id": user_id}
    user = db["users"].find_one(user_id)

    user["elo"] = (user["opponents_total_rating"] + 400 * (user["win"] - user["loss"])) / (user["win"] + user["loss"] + user["draw"])
    user.save()