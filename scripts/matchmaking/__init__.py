global iuq
global rq
iuq = None
rq = []

from scripts.matchmaking.calculaterating import calculaterating
from scripts.matchmaking.unrankedqueue import unrankedqueue
from scripts.matchmaking.rankedqueue import rankedqueue
from scripts.matchmaking.leavequeue import leavequeue
from scripts.matchmaking.endranked import endranked