from scripts.matchmaking import rq
from scripts.matchmaking import iuq

def leavequeue(queue, ip):
    global iuq
    if queue == 0:
        iuq = None
    else:
        rq.remove(next(filter(lambda x: x[2] == ip, rq), None))