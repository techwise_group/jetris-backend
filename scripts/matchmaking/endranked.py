from scripts import db
from scripts.matchmaking import calculaterating

# takes in ids as strings and result is an int
# for result, 1 = defender win, 0 = defender loss, -1 = tie
def endranked(defender_id, attacker_id, result):
    defender_id = {"_id": defender_id} # for cleaner code
    attacker_id = {"_id": attacker_id}
    defender = db["users"].find_one(defender_id) # returns dictionary
    attacker = db["users"].find_one(attacker_id)
    
    # updates total ratings of all opponents
    db["users"].update_one(defender_id, {"$inc": {"opponents_total_rating": attacker["rating"]}})
    db["users"].update_one(attacker_id, {"$inc": {"opponents_total_rating": defender["rating"]}})
    
    # increments win/loss/draw
    if result:
        db["users"].update_one(defender_id, {"$inc": {"win": 1}})
        db["users"].update_one(attacker_id, {"$inc": {"loss": 1}})
    elif result == 0:
        db["users"].update_one(defender_id, {"$inc": {"loss": 1}})
        db["users"].update_one(attacker_id, {"$inc": {"win": 1}})
    else:
        db["users"].update_one(defender_id, {"$inc": {"draw": 1}})
        db["users"].update_one(attacker_id, {"$inc": {"draw": 1}})
    
    calculaterating(defender_id)
    calculaterating(attacker_id)