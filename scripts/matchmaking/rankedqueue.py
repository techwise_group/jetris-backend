from scripts.matchmaking import rq
from scripts import db

def rankedqueue(id, ip, callback):
    elo = db["users"].find_one({"_id": id})["elo"]
    matches = sorted(filter(lambda x: elo * 0.75 <= x[0] <= elo * 4 / 3, rq), key=lambda x: abs(elo - x[0]))
    if len(matches) == 0:
        rq.append([elo, id, ip, callback])
    else:
        callback(*matches[0][1:3])
        matches[0][3](id, ip)
        rq.remove(matches[0])